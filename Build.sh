#!/bin/bash

# Exit if there are errors
set -e

# Kernel version
KERNEL_VERSION=$(make kernelversion)

# Download latest version
git clone git@gitlab.com:GiacomoFicarola/AnyKernel3.git --depth=1
git clone git@github.com:tiann/KernelSU.git --depth=1

# Variables
KERNEL_DEFCONFIG=lmi_defconfig
ANYKERNEL3_DIR=$PWD/AnyKernel3
FINAL_KERNEL_ZIP_NAME=Perf_LMI_v${KERNEL_VERSION}_A14_GiacomoFicarola_Clang-18.0.1.zip

# Old version
old_string=$(sed -n '1p' arch/arm64/configs/lmi_defconfig)
echo "Old string: ${old_string}"
sed -i '1d' arch/arm64/configs/lmi_defconfig

# Preparing new version
config_line='CONFIG_LOCALVERSION="-Perf_LMI_A14_GiacomoFicarola"'
echo "New string: ${config_line}"

# Insert new version
sed -i "1i${config_line}" arch/arm64/configs/lmi_defconfig

# Set path
TC="/home/giacomo/prebuild"

PATH=${TC}/clangLLVM/bin:${TC}/aarch64/bin:${TC}/arm/bin:$PATH

export LLVM=1
export CC=clang
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64
export USE_CCACHE=1

# Optimize compilation process
MAKE="./makeparallel"

# Import device config
make O=out ARCH=arm64 lmi_defconfig

# START
START=$(date +"%s")
make ARCH=arm64 \
        O=out \
        CC=clang \
        AR=llvm-ar \
        LD=ld.lld \
        NM=llvm-nm \
        OBJCOPY=llvm-objcopy \
        OBJDUMP=llvm-objdump \
        STRIP=llvm-strip \
	VERBOSE=1 \
        -j$(nproc --all) Image.gz-dtb dtbo.img

#  Verify dtbo Image
echo -e "**** Verify Image.gz-dtb & dtbo.img ****"
ls $PWD/out/arch/arm64/boot/Image.gz-dtb
ls $PWD/out/arch/arm64/boot/dtbo.img

#Sign kernel


# Is AnyKernel3 present?
echo -e "**** Verifying AnyKernel3 Directory ****"
ls $ANYKERNEL3_DIR

# Remove file
echo -e "**** Removing leftovers ****"
rm -rf $ANYKERNEL3_DIR/Image.gz-dtb
rm -rf $ANYKERNEL3_DIR/dtbo.img
rm -rf $ANYKERNEL3_DIR/$FINAL_KERNEL_ZIP_NAME

# Copy Image.gz-dtb & dtbo.img in AnyKernel3 directory
echo -e "**** Copying Image.gz-dtb & dtbo.img ****"
cp $PWD/out/arch/arm64/boot/Image.gz-dtb $ANYKERNEL3_DIR/
cp $PWD/out/arch/arm64/boot/dtbo.img $ANYKERNEL3_DIR/

# Create zip
echo -e "**** Time to zip up! ****"
cd $ANYKERNEL3_DIR/
zip -r9 $FINAL_KERNEL_ZIP_NAME * -x README $FINAL_KERNEL_ZIP_NAME
cp $ANYKERNEL3_DIR/$FINAL_KERNEL_ZIP_NAME ../$FINAL_KERNEL_ZIP_NAME

# Checksum time
echo -e "**** Kernel is ready, here is your checksum ****"
cd ..
rm -rf $ANYKERNEL3_DIR/$FINAL_KERNEL_ZIP_NAME
rm -rf $ANYKERNEL3_DIR/Image.gz-dtb
rm -rf $ANYKERNEL3_DIR/dtbo.img
rm -rf out/ AnyKernel3/ KernelSU/

END=$(date +"%s")
DIFF=$((END - START))
echo -e '\033[01;32m' "Kernel compiled successfully in $((DIFF / 60)) minute(s) and $((DIFF % 60)) seconds"
sha256sum $FINAL_KERNEL_ZIP_NAME
